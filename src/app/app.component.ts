import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

interface Post {
	title: string;
	content: string;
}

interface PostId extends Post {
	id: string;
}

interface CalendarEvent {
	date: string;
	time: string;
	title: string;
}

interface EventId extends CalendarEvent {
	id: string;
}

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	postsCol: AngularFirestoreCollection<Post>;
	posts: any;

	/*calendarCol: AngularFirestoreCollection<CalendarEvent>;
	events: any;

	calendarDoc: AngularFirestoreDocument<CalendarEvent>;
	event: Observable<CalendarEvent>;

	title: string;
	time: string;
	date: string;*/

	postDoc: AngularFirestoreDocument<Post>;
	post: Observable<Post>;
	
	title: string;
	content: string;
	date: string;

	constructor(private afs: AngularFirestore) {

	}

	ngOnInit() {
		this.postsCol = this.afs.collection('posts');
		//this.postsCol = this.afs.collection('posts', ref => ref.where('date', '<', '11/07/2017').limit(2));
		//this.posts = this.postsCol.valueChanges();
		this.posts = this.postsCol.snapshotChanges()
		.map(actions => {
			return actions.map(a => {
				const data = a.payload.doc.data() as Post;
				const id = a.payload.doc.id;
				return { id, data };
			})
		})
	}

	/*ngOnInit() {
		this.calendarCol = this.afs.collection('Calendar');
		this.events = this.calendarCol.snapshotChanges()
		.map(actions => {
			return actions.map(a => {
				const data = a.payload.doc.data() as CalendarEvent;
				const id = a.payload.doc.id;
				return { id, data };
			})
		})
	}

	addEvent() {
		this.afs.collection('Calendar').add({'date' : this.date, 'time' : this.time, 'title' : this.title});
	}

	getEvent(eventId) {
		this.calendarDoc = this.afs.doc('Calendar/' + eventId);
		this.event = this.calendarDoc.valueChanges();
	}

	deleteEvent(eventId) {
		this.afs.doc('Calendar/' + eventId).delete();
	}*/

	addPost() {
		//this.afs.collection('posts').add({'title' : this.title, 'content' : this.content});
		this.afs.collection('posts').doc('my-custom-id').set({'title' : this.title, 'content' : this.content, 'date' : this.date});
	}

	getPost(postId) {
		this.postDoc = this.afs.doc('posts/'+ postId);
		this.post = this.postDoc.valueChanges();
	}

	deletePost(postId) {
		this.afs.doc('posts/'+ postId).delete();
	}

}
