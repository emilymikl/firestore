import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

interface Event {
	title: string;
	content: string;
}

interface EventId extends Event {
	id: string;
}

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CalendarComponent implements OnInit {
	
	eventsCol: AngularFirestoreCollection<Event>;
	events: any;
	
	title: string;
	time: string;

  	constructor(private afs: AngularFirestore) { 
		
  	}

  	ngOnInit() {
		this.eventsCol = this.afs.collection('CalendarOfEvents');
			this.events = this.eventsCol.snapshotChanges()
			.map(actions => {
				return actions.map(a => {
					const data = a.payload.doc.data() as Event;
					const id = a.payload.doc.id;
					return { id, data };
				})
			})
  	}

}
