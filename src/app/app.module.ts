import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { FormsModule } from '@angular/forms';
import { CalendarComponent } from './calendar/calendar.component';

var firebaseConfig = {    
  apiKey: "AIzaSyAWI4HdJQEnsZ-pSl25NgdVNy2us3SgWdc",
  authDomain: "firestore-1f55d.firebaseapp.com",
  databaseURL: "https://firestore-1f55d.firebaseio.com",
  projectId: "firestore-1f55d",
  storageBucket: "",
  messagingSenderId: "474269563150"
};



@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
